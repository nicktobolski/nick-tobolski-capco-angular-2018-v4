import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * used as a generalized pagination function; @return a subset of the aray passed in
 * Side note: This would be in a shared service module if I had the time.
 * */
export const paginate = (arrToPage: any[] = [], pageSize: number = 8, pageIndex: number = 0): any[] => {
  const start = pageSize * pageIndex;
  const end = pageSize * pageIndex + pageSize;
  return arrToPage.slice(start, end);
};

@Component({
  selector: 'app-cap-table',
  templateUrl: './cap-table.component.html',
  styleUrls: ['./cap-table.component.css']
})
export class CapTableComponent implements OnInit {

  /**
   * Should be an array of like objects containing string values
   */
  @Input() tableData = [];

  /** all columns from tableData @Input */
  get columns() {
    return this.tableData[0] ? Object.keys(this.tableData[0]) : [];
  }
  /** the number of columns from tableData @Input */
  get columnCount() {
    return this.columns.length;
  }
  /** the rows from tableData @Input */
  get rows() {
    return this.tableData.map(record => Object.values(record));
  }

  /** the subset of rows displayed in the table */
  paginatedRows: string[] = [];

  /** The current page */
  _pageIndex = 0;
  set pageIndex(index) {
    this._pageIndex = index;
    this.paginate();
  }
  get pageIndex() {
    return this._pageIndex;
  }

  /** number of pages */
  pagesCount = 0;

  /** The size of the pagination */
  _pageSize = 4;
  set pageSize(newSize) {
    this._pageSize = typeof newSize === 'string' ? parseInt(newSize, 10) : newSize;
    this.pageIndex = 0;
    this.paginate();
  }
  get pageSize() {
    return this._pageSize;
  }

  constructor(private http: HttpClient) {  }
  ngOnInit() {
    this.paginate();
  }

  /** used by Angular for tracking table rows efficiently */
  trackByFn({item}) {
    return item.id;
  }

  /** paginate */
  paginate() {
    this.paginatedRows = paginate(this.rows, this.pageSize, this.pageIndex);
    this.pagesCount = Math.ceil(this.rows.length / this.pageSize);
  }

  /** returns the index of a given property name */
  getPropIndex(propertyName: string): number {
    return Object.keys(this.tableData[0]).indexOf(propertyName);
  }

  // Ideally this would be an output event emitter to the parent component where the parent
  // component would make the http request but I'm doing it here for the sake of speed
  submitRow(index) {
    const postBody = {
      id: this.paginatedRows[index][this.getPropIndex('id')],
      status: this.paginatedRows[index][this.getPropIndex('status')],
    };
    this.http.post(`/api/submit`, postBody).subscribe(
      (res) => alert('wow!'),
      (err) => alert(`Predictable error while submitting ID:${postBody.id} as ${postBody.status}`)
    );
  }

}

