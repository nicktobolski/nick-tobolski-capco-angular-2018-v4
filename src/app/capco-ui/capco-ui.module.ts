import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { CapTableComponent } from './cap-table/cap-table.component';


@NgModule({
  declarations: [CapTableComponent],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [CapTableComponent]
})
export class CapcoUiModule { }
