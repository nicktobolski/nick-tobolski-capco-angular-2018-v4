import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CapcoUiModule } from './capco-ui/capco-ui.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CapcoUiModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
